using UnityEngine;
using UnityEngine.Rendering;

public class PortalManager : MonoBehaviour
{
    public GameObject Cull;

    private GameObject MainCamera;
    private Material[] CullMaterials;

    private void Start() {
        MainCamera = Camera.main.gameObject;
        CullMaterials = Cull.GetComponent<Renderer>().sharedMaterials;
    }

    private void OnTriggerStay(Collider other)
    {
        Vector3 camPositionInPortalSpace = transform.InverseTransformPoint(MainCamera.transform.position);

        Debug.Log("OUCH + " + camPositionInPortalSpace.y);

        if (camPositionInPortalSpace.y < .1f)
        {
            //Disable Stencil Test
            for (int i = 0; i < CullMaterials.Length; i++)
            {
                CullMaterials[i].SetInt("_StencilComp", (int)CompareFunction.Always);
            }
        }
        else
        {
            //Enable Stencil Test
            for (int i = 0; i < CullMaterials.Length; i++)
            {
                CullMaterials[i].SetInt("_StencilComp", (int)CompareFunction.Equal);
            }

        }
    }
}